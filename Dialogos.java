package proyecto;

import javax.swing.JOptionPane;

public class Dialogos extends javax.swing.JDialog{
    public Dialogos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        getComponents();

        //Situamos el JDialog en el centro de la pantalla.
        setLocationRelativeTo(null);
        //No redimensionable.
        setResizable(false);

        //C�digo para confirmar el cierre de la ventana principal.
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent evt){
                if (JOptionPane.showConfirmDialog(rootPane, "�Desea salir de la aplicaci�n?", 
                        "Gestor de clientes", JOptionPane.ERROR_MESSAGE) == JOptionPane.ERROR_MESSAGE){
                    System.exit(0);
                }
            }
        });
    }
}
