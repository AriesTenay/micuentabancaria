package proyecto;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class CuentaBancaria {
	
	
	//crear atributos 
	
	 private String usuario;
	 private float saldo;
	 private String numeroCuenta;
	 private int pass;
	 private int id;
	 private static int IdIncremento=1;
	 
	 
	
	//crear constructores. Se llama de la misma manera que la clase.
	
	

	public CuentaBancaria(String usuario, float saldo, String numeroCuenta, int pass){
		//esto son los argumentos y no es lo mismo
		//que los atributos aunque su nombre sea igual y les pondr� el valor en los par�metro de la clase main.
		
		this.usuario = usuario;
		this.saldo = saldo;
		this.numeroCuenta = numeroCuenta;
		this.pass = pass;
		id=IdIncremento;
		IdIncremento++;
		
		
	}
	//Crear m�todos getter que me devolveran el valor del atributo cuando lo necesite.
	
	public String getUsuario() {
		return usuario;
	}
	public float getSaldo(){
		return saldo;
	}
	public String getNumeroCuenta(){
		return numeroCuenta;
	}
	public int getPass() {
		return pass;
	}
	public int getId() {
		return IdIncremento;
	}
	
	
	//crear m�todos setter que me permitira cambiar el valor de la variable cuando lo necesite.
	
	public void setUsuario(String nuevoUsuario) {//tengo que pasar el valor por argumento.
		this.usuario = nuevoUsuario;
	}
	public void setSaldo(float nuevoSaldo) {
		this.saldo = nuevoSaldo;
		
	}
	public void setNumeroCuenta(String nuevoNumeroCuenta ) {
		this.usuario = nuevoNumeroCuenta;
	}
	//public void setDefaultCloseOperation(int exitOnClose) {
		// setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
	//}
	//M�TODOS ESPEC�FICOS.
	public void ingresarDinero(int cantidad_ingresar) {
		if (cantidad_ingresar>0) {
			this.saldo +=cantidad_ingresar;
		}
			}
	public void sacarDinero(int cantidad_sacar) {
		this.saldo-=cantidad_sacar;
		}
	
	public void getMenu() {
		int opcion =   Integer.parseInt(JOptionPane.showInputDialog(id +" Bienvenida "+ usuario +" tu numero de cuenta es: "+
				  numeroCuenta + " tu saldo es "+ saldo +"�"+
				  "\n�Que operaci�n desea realizar?\n1.Ingresar dinero\n2.Sacar dinero\n3.Salir"));
			

		switch(opcion) {
		  case 1:
			int ingreso=Integer.parseInt(JOptionPane.showInputDialog("�Cu�nto dinero desea ingresar?"));  
			int respuesta=JOptionPane.showConfirmDialog(null, "Desea ingresar "+ingreso,usuario, JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
			    
			    if(respuesta==JOptionPane.YES_OPTION) {
				ingresarDinero(ingreso);
				JOptionPane.showMessageDialog(null, "Tu saldo actual es "+ saldo +"�");
			    }else if(respuesta==JOptionPane.NO_OPTION) {
			  			getMenu();
       	   			  	}else {
       	   			  		JOptionPane.showMessageDialog(null, "Ha salido del programa");
       	   			  	}
			    break;
           
           	case 2:
           		int extraer=Integer.parseInt(JOptionPane.showInputDialog("�Cu�nto dinero desea sacar?"));
           	       int respuesta1=JOptionPane.showConfirmDialog(null, "�Desea extraer ? "+ extraer,usuario, JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);
           	           
           	       if(respuesta1==JOptionPane.YES_OPTION && extraer<this.saldo) {
           	    	sacarDinero(extraer);
      			  JOptionPane.showMessageDialog(null, "Tu saldo es de "+getSaldo()+"�");
      			  
           	       }else if(respuesta1==JOptionPane.YES_OPTION && extraer>this.saldo) {
           	    	   JOptionPane.showMessageDialog(null, "La cantidad a retirar no esta disponble");
           	    	   
           	    	   }else if(respuesta1==JOptionPane.NO_OPTION){
           	    		   getMenu();
           	    	   }else {
           	    		JOptionPane.showMessageDialog(null, "Ha salido del programa");
           	    	   }
           	       break;
		  
			  
		  case 3: 
			  JOptionPane.showMessageDialog(null, "Gracias por visitarnos");
		      break;
		  
		  default : JOptionPane.showMessageDialog(null, "No valido");
	}
	
	
     }
}
